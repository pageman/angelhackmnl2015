package com.hackathon.expressv;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.hackathon.adapter.BucketListAdapter;
import com.hackathon.api.Resource;
import com.hackathon.api.Resource.CallbackArrayListener;
import com.hackathon.api.Resource.CallbackObjectListener;
import com.hackathon.customview.HorizontalListView;
import com.hackathon.pojos.Bucket;
import com.hackathon.pojos.User;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.support.v7.widget.RecyclerView;

public class BucketFragment extends Fragment {

	//

	public interface CallbackFragmentListener {
		void setFeedFragment(Fragment frag);
	}

	private HorizontalListView listView;
	private BucketListAdapter adapter;
	private CallbackFragmentListener callbackFragmentListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View aboutUs = inflater.inflate(R.layout.fragment_bucket, container,
				false);

		listView = (HorizontalListView) aboutUs.findViewById(R.id.bucket_list);
		getBucketList();

		return aboutUs;
	}

	private void getBucketList() {
		String url = "http://192.168.43.144:8080/buckets";
		RequestQueue queue = Volley.newRequestQueue(getActivity());
		Resource res = new Resource(url);
		JsonArrayRequest jsonArrayRequest = res
				.getJsonArrayRequest(new CallbackArrayListener() {

					@Override
					public void getArray(JSONArray jsonArray) {
						// TODO Auto-generated method stub
						Log.d("test", jsonArray.toString());
						Gson gson = new GsonBuilder().create();

						List<Bucket> bucketList = new ArrayList<Bucket>();

						if (jsonArray != null) {
							for (int i = 0; i < jsonArray.length(); i++) {
								try {
									Bucket bucket = gson.fromJson(jsonArray
											.get(i).toString(), Bucket.class);
									bucketList.add(bucket);
								} catch (JsonSyntaxException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}


						adapter = new BucketListAdapter(getActivity(),
								bucketList);
						listView.setAdapter(adapter);
						listView.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {
								Log.d("test", "position: " + position + "id: "+id);
								updateFeed(id);
							}
						});
					}
				});

		queue.add(jsonArrayRequest);
	}

	private void updateFeed(Long id) {
		Fragment frag = new FeedFragment();
		Bundle bundle = new Bundle();
		bundle.putLong("id", id);
		frag.setArguments(bundle);
		callbackFragmentListener.setFeedFragment(frag);

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (activity instanceof CallbackFragmentListener) {
			callbackFragmentListener = (CallbackFragmentListener) activity;
		}

	}

}

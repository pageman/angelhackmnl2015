package com.hackathon.expressv;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;

public class SplashScreen extends Activity {
	
	 private final int SPLASH_DISPLAY_LENGTH = 1000;
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		loadSplashScreen();
	}
	
	private void loadSplashScreen(){
	       new Handler().postDelayed(new Runnable(){
	            @Override
	            public void run() {
	                /* Create an Intent that will start the Menu-Activity. */
	                startActivity(new Intent(SplashScreen.this, DrawerActivity.class));
//	                startActivity(new Intent(SplashScreen.this, MainActivity.class));
	                finish();
	            }
	        }, SPLASH_DISPLAY_LENGTH);
	}
}

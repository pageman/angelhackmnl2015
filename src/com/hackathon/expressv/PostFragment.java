package com.hackathon.expressv;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PostFragment extends Fragment {

	private long id;

	//
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View postFragment = inflater.inflate(R.layout.fragment_post, container,
				false);

		Bundle args = getArguments();
		if (args != null && args.containsKey("id"))
			id = args.getLong("id");
		Log.d("test", "id feed: " + id);
//		if (id == null) {
//			id = (long) 1;
//		}

		return postFragment;
	}

}

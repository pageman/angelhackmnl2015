package com.hackathon.expressv;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.hackathon.adapter.PostListAdapter;
import com.hackathon.api.Resource;
import com.hackathon.api.Resource.CallbackArrayListener;
import com.hackathon.expressv.BucketFragment.CallbackFragmentListener;
import com.hackathon.pojos.Bucket;
import com.hackathon.pojos.Post;

public class FeedFragment extends Fragment{

	private Long id;
	private PostListAdapter adapter;
	private ListView listView;
	private CallbackFragmentListener callbackFragmentListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View feedFragment = inflater.inflate(R.layout.fragment_feed, container,
                false);
        
        listView = (ListView) feedFragment.findViewById(R.id.post_list);

        Bundle args = getArguments();
        if (args  != null && args.containsKey("id"))
            id = args.getLong("id");
        Log.d("test","id feed: " + id);
        if(id==null){
        	id=(long) 1;
        }
        getFeedList();
        
        return feedFragment;
    }
    private void getFeedList() {
		String url = "http://192.168.43.144:8080/buckets/"+id+"/posts";
		   Log.d("test","url: " + url);
		RequestQueue queue = Volley.newRequestQueue(getActivity());
		Resource res = new Resource(url);
		JsonArrayRequest jsonArrayRequest = res
				.getJsonArrayRequest(new CallbackArrayListener() {

					@Override
					public void getArray(JSONArray jsonArray) {
						// TODO Auto-generated method stub
						Log.d("test", jsonArray.toString());
						Gson gson = new GsonBuilder().create();

						List<Post> postList = new ArrayList<Post>();

						if (jsonArray != null) {
							for (int i = 0; i < jsonArray.length(); i++) {
								try {
									Post post = gson.fromJson(jsonArray
											.get(i).toString(), Post.class);
									postList.add(post);
								} catch (JsonSyntaxException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
//
						Log.d("test", "list size: " + postList.size());

						adapter = new PostListAdapter(getActivity(),
								postList,id);
						listView.setAdapter(adapter);
						listView.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> parent,
									View view, int position, long id) {
								Log.d("test", "position: " + position);
								updateFeed(id);
							}
						});
					}
				});

		queue.add(jsonArrayRequest);
	}
    
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (activity instanceof CallbackFragmentListener) {
			callbackFragmentListener = (CallbackFragmentListener) activity;
		}

	}
	
	private void updateFeed(Long id) {
		Fragment frag = new PostFragment();
		Bundle bundle = new Bundle();
		bundle.putLong("id", id);
		frag.setArguments(bundle);
		callbackFragmentListener.setFeedFragment(frag);

	}
    
    
	
}

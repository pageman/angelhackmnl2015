package com.hackathon.expressv;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hackathon.api.Resource;
import com.hackathon.api.Resource.CallbackObjectListener;
import com.hackathon.application.Expressv;
import com.hackathon.pojos.User;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Log.d("test", "main acitivity");
//		getUsers();
		postUser();

	}

	private void getUsers() {
		String url = "http://192.168.43.144:8080/users/1";
		RequestQueue queue = Volley.newRequestQueue(this);
		Resource res = new Resource(url);
		JsonObjectRequest jsObjRequest = res
				.getJsonObjectRequest(new CallbackObjectListener() {
					@Override
					public void getObject(JSONObject jsonObject) {
						Log.d("test", jsonObject.toString());

						Gson gson = new GsonBuilder().create();
						User user = gson.fromJson(jsonObject.toString(),
								User.class);
						Log.d("test",
								"user getFirstName: " + user.getFirstName());
					}
				});
		
		queue.add(jsObjRequest);
//
	}
	
	private void postUser(){
		String url = "http://192.168.43.144:8080/users/4";
		RequestQueue queue = Volley.newRequestQueue(this);
		Resource res = new Resource(url);
		JSONObject jsonBody = new JSONObject();
		try {
			jsonBody.put("id", "4");
			jsonBody.put("email", "test@dj.com");
			jsonBody.put("firstName", "dj");
			jsonBody.put("lastName", "nacpil");
			jsonBody.put("createDate", "1439023957000");
			jsonBody.put("assetId", null);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("test", "response: " + e.toString());
		}
		
		queue.add(res.JsonObjectPost(jsonBody));
	}

}

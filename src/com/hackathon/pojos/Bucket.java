
package com.hackathon.pojos;

import java.util.HashMap;
import java.util.Map;

public class Bucket {

    private Long id;
    private String name;
    private String description;
    private Long assetId;
    private Long createDate;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The assetId
     */
    public Long getAssetId() {
        return assetId;
    }

    /**
     * 
     * @param assetId
     *     The assetId
     */
    public void setAssetId(Long assetId) {
        this.assetId = assetId;
    }

    /**
     * 
     * @return
     *     The createDate
     */
    public Long getCreateDate() {
        return createDate;
    }

    /**
     * 
     * @param createDate
     *     The createDate
     */
    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

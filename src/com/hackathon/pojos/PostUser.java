
package com.hackathon.pojos;

import java.util.HashMap;
import java.util.Map;

public class PostUser {

    private Long id;
    private String title;
    private String content;
    private Long views;
    private Long smiles;
    private Boolean anonymous;
    private Long assetId;
    private User user;
    private Long createDate;
    private Integer chosenComment;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The content
     */
    public String getContent() {
        return content;
    }

    /**
     * 
     * @param content
     *     The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 
     * @return
     *     The views
     */
    public Long getViews() {
        return views;
    }

    /**
     * 
     * @param views
     *     The views
     */
    public void setViews(Long views) {
        this.views = views;
    }

    /**
     * 
     * @return
     *     The smiles
     */
    public Long getSmiles() {
        return smiles;
    }

    /**
     * 
     * @param smiles
     *     The smiles
     */
    public void setSmiles(Long smiles) {
        this.smiles = smiles;
    }

    /**
     * 
     * @return
     *     The anonymous
     */
    public Boolean getAnonymous() {
        return anonymous;
    }

    /**
     * 
     * @param anonymous
     *     The anonymous
     */
    public void setAnonymous(Boolean anonymous) {
        this.anonymous = anonymous;
    }

    /**
     * 
     * @return
     *     The assetId
     */
    public Object getAssetId() {
        return assetId;
    }

    /**
     * 
     * @param assetId
     *     The assetId
     */
    public void setAssetId(Long assetId) {
        this.assetId = assetId;
    }

    /**
     * 
     * @return
     *     The userId
     */
    public User getUserId() {
        return user;
    }

    /**
     * 
     * @param userId
     *     The userId
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * 
     * @return
     *     The createDate
     */
    public Long getCreateDate() {
        return createDate;
    }

    /**
     * 
     * @param createDate
     *     The createDate
     */
    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    /**
     * 
     * @return
     *     The chosenComment
     */
    public Object getChosenComment() {
        return chosenComment;
    }

    /**
     * 
     * @param chosenComment
     *     The chosenComment
     */
    public void setChosenComment(Integer chosenComment) {
        this.chosenComment = chosenComment;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

package com.hackathon.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.hackathon.expressv.R;
import com.hackathon.pojos.Bucket;

public class BucketListAdapter extends BaseAdapter{
	private Context context;
	 private List<Bucket> bucketList = new ArrayList<Bucket>();
	 private AssetManager assetManager;
	public BucketListAdapter(Context context, List<Bucket> bucketList) {
		this.context = context;
		this.bucketList = bucketList;
		assetManager = context.getAssets();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.bucketList.size();
	}

	@Override
	public Bucket getItem(int position) {
		// TODO Auto-generated method stub
		return this.bucketList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return this.bucketList.get(position).getId();
	}

	 @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        final Viewholder viewholder;
	        if (convertView == null) {
	            viewholder = new Viewholder();
	            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = layoutInflater.inflate(R.layout.list_bucket, parent, false);
	            viewholder.bucketIcon = (ImageView) convertView.findViewById(R.id.bucket_icon);
	            convertView.setTag(viewholder);
	        } else {
	            viewholder = (Viewholder) convertView.getTag();
	        }
	        
//	        switch(getItem(position).getAssetId().toString()){
//	        case "1": viewholder.bucketIcon.setBackgroundColor(context.getResources().getColor(R.color.magenta)); break;
//	        case "2": viewholder.bucketIcon.setBackgroundColor(context.getResources().getColor(R.color.pink)); break;
//	        case "3": viewholder.bucketIcon.setBackgroundColor(context.getResources().getColor(R.color.purple)); break;
//	        case "4": viewholder.bucketIcon.setBackgroundColor(context.getResources().getColor(R.color.orange)); break;
//	        case "5": viewholder.bucketIcon.setBackgroundColor(context.getResources().getColor(R.color.blue)); break;
//	        case "6": viewholder.bucketIcon.setBackgroundColor(context.getResources().getColor(R.color.green)); break;
//	        case "7": viewholder.bucketIcon.setBackgroundColor(context.getResources().getColor(R.color.red)); break;
//	        case "8": viewholder.bucketIcon.setBackgroundColor(context.getResources().getColor(R.color.yellow)); break;
//	        }
//	        
	        
			InputStream ims;
			try {
				ims = assetManager.open("bucket_"+getItem(position).getAssetId()+".png");
				Drawable d = Drawable.createFromStream(ims, null);
				viewholder.bucketIcon.setImageDrawable(d);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.d("test", e.toString());
			}
	        
	        
	        return convertView;
	    }


	    static class Viewholder {
	    	ImageView bucketIcon;
	    }

}

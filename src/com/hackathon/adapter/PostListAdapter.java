package com.hackathon.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hackathon.api.Resource;
import com.hackathon.api.Resource.CallbackObjectListener;
import com.hackathon.expressv.R;
import com.hackathon.pojos.Post;
import com.hackathon.pojos.User;

public class PostListAdapter extends BaseAdapter {
	private Context context;
	private List<Post> postList = new ArrayList<Post>();
	private Long id;
	RequestQueue queue;
	AssetManager assetManager;

	public PostListAdapter(Context context, List<Post> bucketList, Long id) {
		this.context = context;
		this.postList = bucketList;
		this.id = id;
		queue = Volley.newRequestQueue(context);
		assetManager = context.getAssets();

		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.postList.size();
	}

	@Override
	public Post getItem(int position) {
		// TODO Auto-generated method stub
		return this.postList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return this.postList.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Viewholder viewholder;
		if (convertView == null) {
			viewholder = new Viewholder();
			LayoutInflater layoutInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.list_feed_adapter,
					parent, false);
			viewholder.name = (TextView) convertView.findViewById(R.id.name);
			viewholder.question = (TextView) convertView
					.findViewById(R.id.question);
			viewholder.views = (TextView) convertView.findViewById(R.id.view);
			viewholder.numberOfSmiles = (TextView) convertView
					.findViewById(R.id.number_smiles);
			viewholder.profilePicture = (ImageView) convertView
					.findViewById(R.id.profile_picture);
			viewholder.postBg = (RelativeLayout) convertView
					.findViewById(R.id.post_bg);
			convertView.setTag(viewholder);
		} else {
			viewholder = (Viewholder) convertView.getTag();
		}
		switch (Integer.parseInt(id.toString())) {
		case 1:
			viewholder.postBg.setBackgroundResource(R.drawable.post_magenta);
			break;
		case 2:
			viewholder.postBg.setBackgroundResource(R.drawable.post_pink);
			break;
		case 3:
			viewholder.postBg.setBackgroundResource(R.drawable.post_violet);
			break;
		case 4:
			viewholder.postBg.setBackgroundResource(R.drawable.post_orange);
			break;
		case 5:
			viewholder.postBg.setBackgroundResource(R.drawable.post_blue);
			break;
		case 6:
			viewholder.postBg.setBackgroundResource(R.drawable.post_green);
			break;
		case 7:
			viewholder.postBg.setBackgroundResource(R.drawable.post_red);
			break;
		case 8:
			viewholder.postBg.setBackgroundResource(R.drawable.post_yellow);
			break;

		default:
			break;
		}

		InputStream ims;
		try {
			ims = assetManager.open("user_"+getItem(position).getUserId()+".png");
			Drawable d = Drawable.createFromStream(ims, null);
			viewholder.profilePicture.setImageDrawable(d);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d("test", e.toString());
		}
	

		viewholder.question.setText(getItem(position).getTitle());
		viewholder.views.setText("Views: "
				+ getItem(position).getViews().toString());
		viewholder.numberOfSmiles.setText(getItem(position).getSmiles()
				.toString());

		String url = "http://192.168.43.144:8080/users/"
				+ getItem(position).getUserId();
		Resource res = new Resource(url);
		JsonObjectRequest jsObjRequest = res
				.getJsonObjectRequest(new CallbackObjectListener() {
					@Override
					public void getObject(JSONObject jsonObject) {
						Log.d("test", jsonObject.toString());

						Gson gson = new GsonBuilder().create();
						User user = gson.fromJson(jsonObject.toString(),
								User.class);
						viewholder.name.setText(user.getFirstName());
					}
				});

		queue.add(jsObjRequest);

		return convertView;
	}

	static class Viewholder {
		TextView name;
		TextView question;
		TextView views;
		TextView numberOfSmiles;
		ImageView profilePicture;
		RelativeLayout postBg;
	}

}

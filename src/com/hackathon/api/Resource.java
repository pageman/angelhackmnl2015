package com.hackathon.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

public class Resource {

	public interface CallbackObjectListener {
		void getObject(JSONObject jsonObject);
	}

	public interface CallbackArrayListener {
		void getArray(JSONArray jsonArray);
	}

	private String url;

	public Resource(String url) {
		this.url = url;
	}

	public JsonObjectRequest getJsonObjectRequest(
			final CallbackObjectListener callbackListener) {
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(
				Request.Method.GET, url, null,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						callbackListener.getObject(response);
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d("test", "response: " + error.toString());
					}
				});
		return jsObjRequest;
	}

	public JsonArrayRequest getJsonArrayRequest(
			final CallbackArrayListener callbackListener) {
		JsonArrayRequest req = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						callbackListener.getArray(response);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				});

		return req;

	}

	public JsonObjectRequest JsonObjectPost(JSONObject jsonBody) {

		JsonObjectRequest jsObjRequest = new JsonObjectRequest(
				Request.Method.PUT, url, jsonBody,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.d("test", "response: " + response.toString());
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d("test", "response: " + error.toString());
					}
				});
		return jsObjRequest;
	}

}
